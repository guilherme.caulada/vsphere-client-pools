# vSphere Client Pools

Esse script cria dois CSVs:

- Um com os pools de clientes do vSphere que possuem CD contendo: CD, Id e Name
- Outro com os recursos do vSphere que não estão nesses pools contendo: CD, Id, Name e ResourcePool

## Usage

```
./vsphere-client-pools.ps1 <VIServer> <User> <Password> <OutputPath>
```
