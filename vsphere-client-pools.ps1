function Get-ResourcePool-ClientCode([Object] $Vm) {
  $match = $Vm.ResourcePool -match "(CD\.?\d\d\d\d\d\d*)"
  if ($match) {
    return $matches[0]
  }
  return ""
}

function Get-Name-ClientCode([Object] $Vm) {
  $match = $Vm.Name -match "(CD\.?\d\d\d\d\d\d*)"
  if ($match) {
    return $matches[0]
  }
  return ""
}

if ($args.length -lt 3) {
  Write-Output "Argument missing! Usage: ./vsphere-client-resources.ps1 <VIServer> <User> <Password> <OutputPath>"
}
else {
  Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $False -Confirm:$False | Out-Null
  Set-PowerCLIConfiguration -InvalidCertificateAction ignore -Confirm:$False | Out-Null
  Connect-VIServer $args[0] -User $args[1] -Password $args[2] | Out-Null
  
  $Vms = Get-VM | Where-Object { -not $(Get-ResourcePool-ClientCode $_) } | 
  Select-Object @{Name = 'CD'; Expression = { Get-Name-ClientCode($_) } }, Id, Name, ResourcePool
  
  $Pools = Get-ResourcePool | Where-Object { $(Get-Name-ClientCode $_) } | 
  Select-Object @{Name = 'CD'; Expression = { Get-Name-ClientCode($_) } }, Id, Name

  $Vms | Export-Csv -NoTypeInformation -Path "$($args[3])\client_resources.csv"
  $Pools | Export-Csv -NoTypeInformation -Path "$($args[3])\client_pools.csv"
}